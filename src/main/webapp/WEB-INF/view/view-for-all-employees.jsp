<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>Info for all employees</h1>

<security:authorize access="hasRole('Admin')">
    <input type="button" value="Salary" onclick="window.location.href='admin-info'"/>
    <div>Only for Admin!!!</div>
</security:authorize>

<security:authorize access="hasRole('SAdmin')">
    <input type="button" value="Performance" onclick="window.location.href='sadmin-info'"/>
    <div>Only for Superadmin!!!</div>
</security:authorize>

</body>
</html>
