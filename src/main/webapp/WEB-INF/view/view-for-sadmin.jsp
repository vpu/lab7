<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f4f4f4;
        }

        h1 {
            color: #333;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
        }

        th, td {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: left;
        }

        th {
            background-color: #4CAF50;
            color: white;
        }

        tr:hover {
            background-color: #f5f5f5;
        }
    </style>
</head>
<body>
<table>
<tr>
    <th>Full name</th>
    <th>Phone number</th>
    <th>Subject taught</th>
</tr>
<c:forEach var="teachers" items="${teachers}">
    <c:url var="updateButton" value="/updateContact">
        <c:param name="contactId" value="${contact.id}"/>
    </c:url>
<tr>
    <td>${teachers.full_name}</td>
    <td>${teachers.phone_number}</td>
    <td>${teachers.subject_taught}</td>
    <td>
        <input type="button" value="update" onclick="window.location.href='${updateButton}'"/>
    </td>
</tr>
</c:forEach>
</table>
<input type="button" value="Create New Contact" onclick="window.location.href='createTeacher'">
</body>
</html>
