package edu.itstep.security.entity;

import javax.validation.constraints.*;
import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "teachers")
public class Teacher {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "Full name")
    @NotBlank
    @Size(min = 2)
    @Pattern(regexp = "[a-zA-Z]*")
    private String fullName;

    @Column(name = "phone_number")
    @NotBlank
    @Pattern(regexp = "0\\d{2}-\\d{3}-\\d{2}-\\d{2}")
    private String phone;

    @Column(name = "subject_taught")
    @NotBlank
    private String subject_taught;

    public Teacher(){

    }

    public Teacher(String fullName, String phone, String subject_taught){
        this.fullName = fullName;
        this.phone = phone;
        this.subject_taught = subject_taught;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setSubject_taught(String subject_taught) {
        this.subject_taught = subject_taught;
    }

    public String getFullName() {
        return fullName;
    }

    public String getPhone() {
        return phone;
    }

    public String getSubject_taught() {
        return subject_taught;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Teacher teacher = (Teacher) o;
        return id == teacher.id && Objects.equals(fullName, teacher.fullName) && Objects.equals(phone, teacher.phone) && Objects.equals(subject_taught, teacher.subject_taught);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, fullName, phone, subject_taught);
    }

    @Override
    public String toString() {
        return "Teacher{" +
                "id=" + id +
                ", fullname='" + fullName + '\'' +
                ", phone='" + phone + '\'' +
                ", subject_taught='" + subject_taught + '\'' +
                '}';

    }
