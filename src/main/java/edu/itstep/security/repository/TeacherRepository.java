package edu.itstep.security.repository;

import edu.itstep.security.entity.Teacher;

import java.util.List;

public interface TeacherRepository {
    List<Teacher> getAll();

    void saveOrUpdate(Teacher contact);

}
