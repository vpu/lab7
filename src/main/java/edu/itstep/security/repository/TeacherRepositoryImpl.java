package edu.itstep.security.repository;

import edu.itstep.security.entity.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public class TeacherRepositoryImpl implements TeacherRepository{
    @Autowired
    private SessionFactory sessionFactory;
    @Override
    @Transactional
    public List<Teacher> getAll() {
        return sessionFactory
                .getCurrentSession()
                .createQuery("from Teachers", Teacher.class)
                .getResultList();
    }

    @Override
    @Transactional
    public void saveOrUpdate(Teacher teacher) {
        sessionFactory
                .getCurrentSession()
                .saveOrUpdate(teacher);
    }
}
