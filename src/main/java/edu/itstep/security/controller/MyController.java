package edu.itstep.security.controller;

import edu.itstep.security.entity.Teacher;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class MyController {

    @GetMapping("/")
    public String getInfoForAllEmployees(){
        return "view-for-all-employees";
    }

    @GetMapping("/admin-info")
    public String getInfoForAdmin(){
        return "view-for-admin";
    }

    @GetMapping("/SAdmin-info")
    public String getInfoForSAdmin(){
        return "view-for-sadmin";
    }

    @RequestMapping("/")
    public String showAll(Model model) {
        List<Teacher> contacts = TeacherRepository.getAll();
        model.addAttribute("teacher", teacher);
        return "view-for-sadmin";
    }

    @RequestMapping("/createTeacher")
    public String createTeacher(Model model) {
        model.addAttribute("teacher", new Teacher());
        return "createTeacher";
    }

    @RequestMapping("/saveTeacher")
    public String saveTeacher(@ModelAttribute("contact") @Valid Teacher teacher, BindingResult result) {
        if (result.hasErrors()) {
            return "view-for-sadmin"; // повернення до форми, якщо є помилки
        }
        System.out.println(teacher);
        TeacherRepository.saveOrUpdate(teacher);
        return "redirect:/";
    }

    @RequestMapping("/updateContact")
    public String updateTeacher(@RequestParam("teacherId") int id, Model model) {
        Teacher teacher = TeacherRepository.getById(id);
        model.addAttribute("teacher", teacher);
        return "view-for-sadmin";
    }
}
